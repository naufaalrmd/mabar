import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'

export default class FieldsController {

	public async store({request, response, params}: HttpContextContract) {

		const venuesId = params['venue_id']
        const user = new Field()
        user.name = request.input('name')
        user.type = request.input('type')
        user.venues_id = venuesId

        await user.save()
        console.log(user.$isPersisted)
     	return response.created({message:"Success"})
	}
	public async index({response}:HttpContextContract) {
		let fields = await Field.all()
		return response.created({message:'secc', fields})
	}
}
