import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'
import User from 'App/Models/User'

export default class BookingsController {

	public async store ({request, response, params, auth}:HttpContextContract) {

		// const venueId = params['venue_id']
		const session_user = auth.user
		const id  = session_user?.id
		

		let play_date_start = request.input('play_date_start')
		let play_date_end = request.input('play_date_end')
		let fields_id = request.input('fields_id')
		let user_id_booking:number = id

		const user = new Booking()
		user.play_date_start = play_date_start
		user.play_date_end = play_date_end
		user.user_id_booking = user_id_booking
		user.fields_id = fields_id
		await user.save()
        console.log(user.$isPersisted)
     	return response.created({message:"Success"}) 
	}

	public async index ({response}: HttpContextContract) {
		let bookings = await Booking.all()
		return response.created({Message: 'Success all bookings', bookings})
	}

	public async show ({response, params, auth}: HttpContextContract) {

		let booking = await Booking.find(params.id)
		return response.created({Message: 'Get by Id', booking})

	}

	public async join ({request, response, params, auth}:HttpContextContract) {

		const session_user = auth.user
		const id = session_user?.id
		let user = await User.find(id)
		let booking = await Booking.find(params.id)
		if (booking != null && user!= null) {
      		await booking.related('usershasbooking').attach([user.id])
    	}	

    	return response.created({message: 'Success join', booking})

	}

}
