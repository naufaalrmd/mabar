import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Venue from 'App/Models/Venue'
import Booking from 'App/Models/Booking'
export default class VenuesController {

	public async index ({response}: HttpContextContract) {

		let venues = await Venue.query().preload('fieldes')
		return response.created({Message: 'Success', venues})

	}

	public async store ({request, response}: HttpContextContract) {

		
		let name = request.input('name')
		let phone= request.input('phone')
		let address=request.input('address')

		const venue_store = await Venue.create({
			name,
			phone,
			address
		})
			
		return response.created({Message: 'Success Post', venue_store})

	}	

	public async show ({response, params}:HttpContextContract) {

		// let venue = await Venue.find(params.id)
		let venue = await Venue.query().where('id', params.id).preload('bookings').first()
		return response.created({Message: 'Get by Id', venue})
	}

	public async update ({ request, response, params}: HttpContextContract) {
		const venue = await Venue.findOrFail(params.id)
		venue.name = request.input('name')
		venue.address = request.input('address')
		venue.phone = request.input('phone')
		await venue.save()

		response.created({Message: 'update', venue})


	}


}
