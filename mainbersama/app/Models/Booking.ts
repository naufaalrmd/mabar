import { DateTime } from 'luxon'
import { BaseModel, column, manyToMany, ManyToMany, belongsTo, BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import Venue from 'App/Models/Venue'
import Field from 'App//Models/Field'
import User from 'App//Models/User'
export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public play_date_start: DateTime

  @column()
  public play_date_end:  string

  @column()
  public user_id_booking: number

  @column()
  public fields_id: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=> Field, {
    foreignKey : 'fields_id'
  } )
  public field : BelongsTo <typeof Field>

  @belongsTo(()=> User, {
    foreignKey : 'users_id'
  } )
  public user : BelongsTo <typeof User>

  @manyToMany(()=> User, {
    pivotTable: 'users_has_bookings',
    localKey: 'id',
    pivotForeignKey: 'bookings_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'users_id',
  })
  public usershasbooking: ManyToMany <typeof User>

}
