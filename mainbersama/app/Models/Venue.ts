import { DateTime } from 'luxon'
import { BaseModel, column, hasMany, HasMany, manyToMany, ManyToMany,hasManyThrough, HasManyThrough } from '@ioc:Adonis/Lucid/Orm'
import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'

export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public phone: string

  @column()
  public address: string

  @column()
  public usersId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Field,{
    foreignKey: 'venues_id'
  })
  public fieldes: HasMany<typeof Field>

  // @hasMany(() => Booking, {
  //   foreignKey: 'fields_id'
  // })
  // public bookings: HasMany<typeof Booking>

   @hasManyThrough([() => Booking, () =>Field], {
    localKey: 'id',
    foreignKey: 'venues_id',
    throughLocalKey: 'id',
    throughForeignKey: 'fields_id'
  })
  public bookings: HasManyThrough<typeof Booking>


}
