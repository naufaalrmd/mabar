import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.dateTime('play_date_start')
      table.string('play_date_end')
      table.increments('id')
      table.integer('user_id_booking').notNullable().unsigned().references('id').inTable('users')
      table.integer('fields_id').notNullable().unsigned().references('id').inTable('fields')
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
