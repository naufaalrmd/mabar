/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

Route.get('health', async ({ response }) => {
  const report = await HealthCheck.getReport()
  
  return report.healthy
    ? response.ok(report)
    : response.badRequest(report)
})




Route.group(() => {

	Route.get('/', async () => {

  		return { hello: 'world' }

	}).as('home')
	Route.resource('fields', 'FieldsController').apiOnly()
	Route.resource('venues', 'VenuesController')
	Route.resource('venues.fields', 'FieldsController').apiOnly()
	Route.resource('venues.booking', 'BookingsController').only(['store'])
	Route.resource('bookings', 'BookingsController')
	Route.post('/bookings/:bookings_id/join', 'BookingsController.join').as('join')
	Route.post('register', 'AuthController.register').as('register')
	Route.post('login', 'AuthController.login').as('login')
	Route.post('otpConfirmation', 'AuthController.otpConfirmation').as('otp.conformation')




}).prefix('/api/v1').as('apiv1')

Route.get('/api/hello', 'TestsController.hello').as('test')
Route.post('register', 'AuthController.register').as('register')